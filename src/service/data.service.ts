import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
    ) { }

  login(username: String, password: String) {
    let body = {
      "username": username,
      "password": password
    }
    return this.http.post<any>(`${environment.apiUrl}login`, body);
  }

  register(username: String, password: String) {
    let body = {
      "username": username,
      "password": password
    }
    return this.http.post<any>(`${environment.apiUrl}login/signup`, body);
  }

  getAccounts() {
    return this.http.get<any>(`${environment.apiUrl}administrator/getaccounts`);
  }

  editAccount(body) {
    return this.http.put<any>(`${environment.apiUrl}administrator/update/${body.username}`, body);
  }

  addAccount(body) {
    return this.http.post<any>(`${environment.apiUrl}administrator/addaccount`, body);
  }

  deleteAccount(id) {
    return this.http.delete<any>(`${environment.apiUrl}administrator/deleteaccount/${id}`);
  }

  checkRestaurant() {
    const id = this.auth.getId();
    return this.http.post<any>(`${environment.apiUrl}managerpage/haverestaurant?id=${id}`, {});
  }

  getProducts() {
    const id = this.auth.getId();
    return this.http.get<any>(`${environment.apiUrl}managerpage/viewproducts?id=${id}`);
  }

  getMenus() {
    const id = this.auth.getId();
    return this.http.get<any>(`${environment.apiUrl}managerpage/viewmenus?id=${id}`);
  }

  addMenu(body) {
    const id = this.auth.getId();
    return this.http.post<any>(`${environment.apiUrl}managerpage/addmenu?id=${id}`, body);
  }

  addProduct(body) {
    const id = this.auth.getId();
    return this.http.post<any>(`${environment.apiUrl}managerpage/addproduct?id=${id}`, body);
  }

  addProductToMenu(idProd, idMenu) {
    return this.http.post<any>(`${environment.apiUrl}managerpage/addproductonmenu?productId=${idProd}&menuId=${idMenu}`, {});
  }

  viewMenuProducts(idMenu) {
    return this.http.post<any>(`${environment.apiUrl}managerpage/viewmenuproduct?menuId=${idMenu}`, {});
  }

  addSale(idMenu, sale) {
    return this.http.post<any>(`${environment.apiUrl}managerpage/addreducere?idmenu=${idMenu}&?reducere=${sale}`, {});
  }

  addRestaurant(body) {
    const id = this.auth.getId();
    return this.http.post<any>(`${environment.apiUrl}managerpage/addrestaurant?id=${id}`, body);
  }

  deleteMenu(id) {
    return this.http.post<any>(`${environment.apiUrl}managerpage/deletemenu?id=${id}`, {});
  }

  deleteProductFromRestaurant(id) {
    return this.http.post<any>(`${environment.apiUrl}managerpage/deleteproductfromrestaurant?id=${id}`, {});
  }

  deleteProductFromMenu(idProd, idMenu) {
    return this.http.post<any>(`${environment.apiUrl}managerpage/deleteproductfrommenu?productId=${idProd}&menuId=${idMenu}`, {});
  }

  addaugaReducere(idProd, reducere) {
    return this.http.post<any>(`${environment.apiUrl}managerpage/addreducere?idProd=${idProd}&reducere=${reducere}`, {});
  }

  donnorViewRestaurants() {
    return this.http.get<any>(`${environment.apiUrl}donnorpage/viewrestaurant`);
  }

  donnorRestaurantMenu(idRestaurant) {
    return this.http.get<any>(`${environment.apiUrl}donnorpage/viewmenuonrestaurant?idRestaurant=${idRestaurant}`);
  }

  donnorViewMenuProducts(idMenu) {
    return this.http.post<any>(`${environment.apiUrl}donnorpage/viewmenuproduct?menuId=${idMenu}`, {});
  }

  donnorMakeOrder(body, type = 1) {
    return this.http.post<any>(`${environment.apiUrl}donnorpage/makeorder?typeComanda=${type}`, body);
  }

  donnorViewproductsonrestaurant(idRestaurant) {
    return this.http.get<any>(`${environment.apiUrl}donnorpage/viewproductsonrestaurant?restaurantId=${idRestaurant}`);
  }

  donnorGetPersoane() {
    return this.http.post<any>(`${environment.apiUrl}donnorpage/persoaneneajutorateAndwish`, {});
  }

  disGetProducts() {
    return this.http.get<any>(`${environment.apiUrl}disadvantaged/viewproducts`);
  }

  disGetWishlist() {
    const username = this.auth.getUser();
    return this.http.get<any>(`${environment.apiUrl}disadvantaged/viewwishlist?accUsername=${username}`);
  }

  disAddWishList(product) {
    const username = this.auth.getUser();
    return this.http.post<any>(`${environment.apiUrl}disadvantaged/addwishlist?productId=${product}&accUsername=${username}`, product);
  }

  disViewComanda(username) {
    return this.http.post<any>(`${environment.apiUrl}disadvantaged/viewcomanda?username_persoana=${username}`, {});
  }

  disAddAllergy(product) {
    const username = this.auth.getUser();
    return this.http.post<any>(`${environment.apiUrl}disadvantaged/addalergies?productId=${product}&accUsername=${username}`, product);
  }

  disGetOrders() {
    const username = this.auth.getUser();
    return this.http.post<any>(`${environment.apiUrl}disadvantaged/viewcomanda?username_persoana=${username}`, {});
  }

  disGetAllergies() {
    const username = this.auth.getUser();
    return this.http.post<any>(`${environment.apiUrl}disadvantaged/getallergies?accUsername=${username}`, {});
  }

  disAcceptOrder() {
    const username = this.auth.getUser();
    return this.http.post<any>(`${environment.apiUrl}disadvantaged/acceptaComanda?username_persoana=${username}`, {});
  }

  disOrderRecieved() {
    const username = this.auth.getUser();
    return this.http.post<any>(`${environment.apiUrl}disadvantaged/comandaPrimita?username_persoana=${username}`, {});
  }

}
