import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthService } from 'src/service/auth.service';
import { CartService } from 'src/service/cart.service';
import { DataService } from 'src/service/data.service';
import { CartComponent } from '../cart/cart.component';

@Component({
  selector: 'app-donor-page',
  templateUrl: './donor-page.component.html',
  styleUrls: ['./donor-page.component.scss']
})
export class DonorPageComponent implements OnInit {

  restaurants = [];
  openedSideNav: boolean = false;
  restaurantId = 0;
  modalRef: BsModalRef;

  constructor(
    private dataService: DataService,
    private cartService: CartService,
    private auth: AuthService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.dataService.donnorViewRestaurants().subscribe(
      (result)=> {
        this.restaurants = result;
      }
    );
  }

  selectRestaurant(id) {
    this.restaurantId = id;
    console.log('restaurant' + this.restaurantId);
    this.openedSideNav = true;
  }

  viewCart() {
    this.cartService.printStuff();
    this.modalService.show(CartComponent);
  }

  logout() {
    this.auth.logout();
  }

}
