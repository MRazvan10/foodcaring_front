import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
  } from '@angular/common/http'
  import { Injectable } from '@angular/core'
  import { Observable, throwError } from 'rxjs'
  import { catchError } from 'rxjs/operators'
  
  import { AuthService } from './auth.service'
  
  @Injectable()
  export class RequestHttpInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      if (req.url.includes('/login')) {
        return next.handle(req);
      }
      const jwt = this.authService.getToken()
      const request = req.clone(
        { setHeaders: 
          { authorization: `Bearer ${jwt}`}
        });
      return next.handle(request).pipe(
        catchError((err) => {
          return throwError(err)
        })
      )
    }
  }