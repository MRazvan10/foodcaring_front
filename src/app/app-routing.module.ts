import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { AuthGuard } from './auth.guard';
import { DisadvantagedComponent } from './disadvantaged/disadvantaged.component';
import { DonorPageComponent } from './donor-page/donor-page.component';
import { LoginComponent } from './login/login.component';
import { ManagerPageComponent } from './manager-page/manager-page.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent }, 
  { path: 'register', component: RegisterComponent },
  { path: 'manager', component: ManagerPageComponent, canActivate: [AuthGuard] },
  { path: 'admin', component: AdminPageComponent, canActivate: [AuthGuard] },
  { path: 'donor', component: DonorPageComponent, canActivate: [AuthGuard] },
  { path: "disadvantaged", component: DisadvantagedComponent, canActivate: [AuthGuard]},
  { path: '',   redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
