import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { reduce } from 'rxjs/operators';
import { AuthService } from 'src/service/auth.service';
import { DataService } from 'src/service/data.service';
import { MenuModalComponent } from '../menu-modal/menu-modal.component';
import { ProductModalComponent } from '../product-modal/product-modal.component';

@Component({
  selector: 'app-manager-page',
  templateUrl: './manager-page.component.html',
  styleUrls: ['./manager-page.component.scss']
})
export class ManagerPageComponent implements OnInit {

  configuredRestaurant = false;
  restaurantForm: FormGroup;
  products = [];
  menus = [];
  bsModalRef: BsModalRef;
  loadingProd = false;
  loadingMenu = false;
  discount;

  constructor(
    private dataService: DataService,
    private fb: FormBuilder,
    private modalService: BsModalService,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.loadingProd=true;
    this.dataService.checkRestaurant().subscribe(
      (result) => {
        this.loadingProd=false;
        if(result === 'TRUE') {
          this.configuredRestaurant = true;
          this.refresh();
        }
        else {
          this.configuredRestaurant = false;
        }
      });
    this.restaurantForm = this.fb.group({
      restaurant_name: ['', Validators.required],
      address: ['', Validators.required]
    });
  }

  submitRestaurant() {
    const body = this.restaurantForm.value;
    console.log(body);
    this.dataService.addRestaurant(body).subscribe(
      (result) => {
        console.log(result);
        this.configuredRestaurant = true;
        this.refresh();
      }
    );
  }

  refresh() {
    this.loadingProd = true;
    this.loadingMenu = true;
    this.dataService.getProducts().subscribe(
      (result) => {
        this.products = result;
        this.loadingProd = false;
      }
    );
    this.dataService.getMenus().subscribe(
      (result) => {
        this.menus = result;
        for(let menu of this.menus) {
          this.dataService.viewMenuProducts(menu.menu_id).subscribe(
            (result) => {
              menu.products = result;
            });
          }
        this.loadingMenu = false;
      }
    );
  }

  addProduct() {
    this.bsModalRef = this.modalService.show(ProductModalComponent);
    this.bsModalRef.onHidden.subscribe(
      () => {
        this.refresh();
      }
    )
  }
  
  editProduct(product) {
    const initialState = {
      product: product,
      edit: true
    };
    this.bsModalRef = this.modalService.show(ProductModalComponent, {initialState});
  }

  deleteProduct(id) {
    this.dataService.deleteProductFromRestaurant(id).subscribe(
      () => {
        this.refresh();
      }
    );
  }

  addMenu() {
    const initialState = {add: true};
    this.bsModalRef = this.modalService.show(MenuModalComponent, {initialState});
    this.bsModalRef.onHidden.subscribe(
      () => {
        this.refresh();
      }
    );
  }

  addProductMenu(id) {
    const initialState = {addProduct: true, menuId: id };
    this.bsModalRef = this.modalService.show(MenuModalComponent, {initialState});
    this.bsModalRef.onHidden.subscribe(
      () => {
        this.refresh();
      }
    );
  }

  removeProductMenu(id) {
    const initialState = {deleteProduct: true, menuId: id };
    this.bsModalRef = this.modalService.show(MenuModalComponent, {initialState});
    this.bsModalRef.onHidden.subscribe(
      () => {
        this.refresh();
      }
    );
  }
  
  deleteMenu(id) {
    this.dataService.deleteMenu(id).subscribe(
      () => {
        this.refresh();
      }
    );
    
  }

  logout() {
    this.auth.logout();
  }

  makeDiscount(idMenu) {
    console.log(this.discount)
    this.dataService.addaugaReducere(idMenu, this.discount).subscribe(
      (result)=> {
        console.log(result);
      }
    )
  }

}
