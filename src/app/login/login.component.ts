import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/service/auth.service';
import { CartService } from 'src/service/cart.service';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  loginInvalid: boolean;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private dataService: DataService,
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    this.dataService.login(this.form.get('username').value, this.form.get('password').value).subscribe(response => {
      console.log(response)
      this.auth.setToken(response.token);
      this.manageRouting();
    });
  }

  manageRouting() {
    console.log(this.auth.getRole());
    if (this.auth.getRole() === 'MANAGER') {
      this.router.navigate(['/manager']);
    } else if (this.auth.getRole() === 'ADMIN') {
      this.router.navigate(['/admin']);
    } else if (this.auth.getRole() === 'DONNOR') {
      this.cartService.setDonator(this.form.get('username').value);
      this.router.navigate(['/donor']);
    } else if (this.auth.getRole() === 'DISADVANTAGED') {
      this.auth.setUser(this.form.get('username').value);
      this.router.navigate(['/disadvantaged']);
    }
  }

}
