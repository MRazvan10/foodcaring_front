import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CartService } from 'src/service/cart.service';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'modal-content',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  products = [];
  menus = [];
  disadvantagedWishList = [];
  disadvantagedUsername = '';

  constructor(public bsModalRef: BsModalService,
    private cart: CartService,
    private dataService: DataService) { }

  ngOnInit(): void {
    this.products = this.cart.getProducts();
    this.menus = this.cart.getMenus();
    this.dataService.donnorGetPersoane().subscribe(
      (result) => {
        this.disadvantagedWishList = result; 
      }
    );
  }

  submit() {
    if(this.disadvantagedUsername) {
      this.cart.setPersoana(this.disadvantagedUsername);
    }
    this.cart.trimiteComanda();
    this.bsModalRef.hide();
  }

  getDisadvantaged() {
    return Object.keys(this.disadvantagedWishList);
  }

  removeMenu(menu) {
    this.cart.removeMenu(menu);
    this.products = this.cart.getProducts();
    this.menus = this.cart.getMenus();
  }

  removeProduct(product) {
    this.cart.removeProduct(product);
    this.products = this.cart.getProducts();
    this.menus = this.cart.getMenus();
  }

}
