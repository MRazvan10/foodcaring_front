import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'app-menu-modal',
  templateUrl: './menu-modal.component.html',
  styleUrls: ['./menu-modal.component.scss']
})
export class MenuModalComponent implements OnInit {

  add: boolean = false;
  addProduct: boolean = false;
  deleteProduct: boolean = false;
  productId: number;
  menuId: number;
  menuForm: FormGroup;

  constructor(
    public bsModalRef: BsModalRef,
    private fb: FormBuilder,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.menuForm = this.fb.group({
      menu_name: ['', Validators.required],
      price: ['', Validators.required],
      });
  }

  submit() {
    console.log(this.menuId);
    console.log(this.addProduct);
    console.log(this.deleteProduct);
    console.log(this.add);
    if(this.add) {
      const body = this.menuForm.value;
      this.dataService.addMenu(body).subscribe(
        () => {
          this.bsModalRef.hide()
        }
      ); 
    }
    if(this.addProduct) {
      this.dataService.addProductToMenu(this.productId, this.menuId).subscribe(
        () => {
          this.bsModalRef.hide()
        }
      );
    }
    if(this.deleteProduct) {
      console.log('delProd')
      this.dataService.deleteProductFromMenu(this.productId, this.menuId).subscribe(
        () => {
          this.bsModalRef.hide()
        }
      );;
    }
  }
}
