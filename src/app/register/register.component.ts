import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/service/auth.service';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  passwordInvalid: boolean = false;
  error: String;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      password2: ['', Validators.required]
    });
  }

  onSubmit() {
    if(this.form.get('password').value === this.form.get('password2').value) {
      this.dataService.register(this.form.get('username').value, this.form.get('password').value).subscribe();
      this.router.navigate(['/login']);
    }
    else {
      this.passwordInvalid = true;
    }
  }


}
