import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Router } from '@angular/router';
import { AuthService } from 'src/service/auth.service';
import { DataService } from 'src/service/data.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { EditAccountModalComponent } from '../edit-account-modal/edit-account-modal.component';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AdminPageComponent implements OnInit {
    dataSource = [];
    columnsToDisplay = ['account_id', 'username', 'role'];
    expandedElement: any | null;
    bsModalRef: BsModalRef;

    constructor(
      private http: HttpClient,
      private router: Router,
      private auth: AuthService,
      private dataService: DataService,
      private modalService: BsModalService
    ) {
  
    }
  
    ngOnInit() {
      this.refreshTable();
    }
  
    refreshTable() {
      const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Authorization': `Bearer ${this.auth.getToken()}`
      }
  
      const httpOptions = {
        headers: new HttpHeaders(headers)
      };
      this.dataService.getAccounts().subscribe(
        (result) => {
          console.log(result);
          this.dataSource = result;
        }
      );
    }

    add () {
      this.bsModalRef = this.modalService.show(EditAccountModalComponent);
      this.bsModalRef.onHidden.subscribe(
        () => {
          this.refreshTable()
        }
      );
    }
  
    edit (element) {
      const initialState = {
        edit: true,
        username: element.username
      };
      this.bsModalRef = this.modalService.show(EditAccountModalComponent, {initialState});
      this.bsModalRef.onHidden.subscribe(
        () => {
          this.refreshTable()
        }
      );
    }
  
    delete(element) {
      this.dataService.deleteAccount(element.account_id).subscribe(
        (result) => {
          console.log(result);
          this.refreshTable();
        }
      );
    }

    logout() {
      this.auth.logout();
    }
}
