import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.scss']
})
export class ProductModalComponent implements OnInit {

  product;
  edit;
  productForm: FormGroup;

  constructor(
    public bsModalRef: BsModalRef,
    private fb: FormBuilder,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.productForm = this.fb.group({
      product_name: [this.product?.product_name, Validators.required],
      pic: [this.product?.pic, Validators.required],
      product_price: [this.product?.product_price, Validators.required],
      product_description: [this.product?.product_description, Validators.required]
    });
  }

  submit() {
    const body = this.productForm.value;
    this.dataService.addProduct(body).subscribe(
      () => {
        this.bsModalRef.hide()
      }
    );
  }



}
