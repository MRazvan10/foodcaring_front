import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { CartService } from 'src/service/cart.service';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'app-restaurant-nav',
  templateUrl: './restaurant-nav.component.html',
  styleUrls: ['./restaurant-nav.component.scss']
})
export class RestaurantNavComponent implements OnInit, OnChanges {
  @Input() id;
  products = [];
  menus = [];
  loadingProd = false;
  loadingMenu = false;

  constructor(
    private dataService: DataService,
    private cartService: CartService) { }

  ngOnInit() {

  }

  ngOnChanges(): void {
    console.log(this.id);
    this.refresh();
  }

  refresh() {
    this.loadingMenu = true;
    this.loadingProd = true;
    this.dataService.donnorViewproductsonrestaurant(this.id).subscribe(
      (result) => {
        this.products = result;
        this.loadingProd = false;
      }
    );
    this.dataService.donnorRestaurantMenu(this.id).subscribe(
      (result) => {
        this.menus = result;
        for(let menu of this.menus) {
          this.dataService.donnorViewMenuProducts(menu.menu_id).subscribe(
            (result) => {
              menu.products = result;
            });
          }
        this.loadingMenu = false;
      }
    );
  }

  addToCartMenu(menu) {
    alert("Menu added to card");
    this.cartService.addMenu(menu);
  }

  addToCartProduct(product) {
    alert("Product added to card");
    this.cartService.addProduct(product);
  }



}
