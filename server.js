const express = require('express');
const path = require('path');

const app = express();

app.use(express.static('./dist/FoodCaring'));

app.get('/*', function(req, res) {
  res.sendFile('index.html', {root: 'dist/FoodCaring/'}
);
});

app.listen(process.env.PORT || 4200);
