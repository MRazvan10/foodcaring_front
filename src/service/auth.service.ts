import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private storageKey = 'token';

  constructor(
    private router: Router
  ) { }
  setToken(token: string) {
    localStorage.setItem(this.storageKey, token);
  }

  getToken() {
    return localStorage.getItem(this.storageKey);
  }

  isLoggedIn() {
    return this.getToken() !== null;
  }

  getRole() {
    let decoded: any = jwt_decode(this.getToken());
    return decoded.roles.authority;
  }

  getId() {
    let decoded: any = jwt_decode(this.getToken());
    return decoded.sub;
  }

  setUser(username) {
    localStorage.setItem('user', username);
  }

  getUser() {
    return localStorage.getItem('user');
  }

  logout() {
    localStorage.removeItem(this.storageKey);
    this.router.navigate(['/login']);
  }

}
