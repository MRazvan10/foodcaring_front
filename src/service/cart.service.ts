import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  menus;
  products;
  persoana: string;

  constructor(
    private dataService: DataService
  ) {
    this.menus = [];
    this.products = [];
  }

  addMenu(menu) {
    this.menus.push(menu);
  }

  removeMenu(menu) {
    const index = this.menus.indexOf(menu);
    if(index > -1) {
      this.menus.splice(index, 1);
    }
  }

  addProduct(product) {
    this.products.push(product);
  }

  removeProduct(product) {
    const index = this.products.indexOf(product);
    if(index > -1) {
      this.products.splice(index, 1);
    }
  }

  setDonator(username) {
    localStorage.setItem('user', username);
  }

  getDonator() {
    return localStorage.getItem('user');
  }

  setPersoana(username) {
    this.persoana = username;
  }

  getMenus() {
    return this.menus;
  }

  getProducts() {
    return this.products;
  }

  trimiteComanda() {
    const body = {
      'listmenuid': this.menus.map((m) => m.menu_id),
      'productid': this.products.map((p) => p.product_id),
      'username_donator': this.getDonator(),
      'username_persoana': this.persoana
    }
    if(this.persoana == null || !this.persoana.length) {
      this.dataService.donnorMakeOrder(body).subscribe(
        (result) => {
          console.log(result);
        });
    }
    else {
      this.dataService.donnorMakeOrder(body, 0).subscribe(
        (result) => {
          console.log(result);
        });
    }
    this.menus = [];
    this.products = [];
    this.setPersoana(null);
  }

  printStuff() {
    const body = {
      'listmenuid': this.menus,
      'productid': this.products,
      'username_donator': this.getDonator(),
      'username_persoana': this.persoana
    }
    console.log(body);
  }
}
