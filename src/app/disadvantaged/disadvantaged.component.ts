import { Component, OnInit, ɵɵqueryRefresh } from '@angular/core';
import { AuthService } from 'src/service/auth.service';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'app-disadvantaged',
  templateUrl: './disadvantaged.component.html',
  styleUrls: ['./disadvantaged.component.scss']
})
export class DisadvantagedComponent implements OnInit {


  products = [];
  wishlist = [];
  order;
  alergies = [];

  constructor(private dataService: DataService,
    private auth: AuthService) { }

  ngOnInit(): void {
    this.refresh();
  }

  refresh() {
    this.dataService.disGetProducts().subscribe(
      (result) => {
        this.products = result;
      }
    );
    this.dataService.disGetWishlist().subscribe(
      (result) => {
        this.wishlist = result;
      }
    );
    this.dataService.disGetOrders().subscribe(
      (result) => {
        this.order = result;
      }
    );
    this.dataService.disGetAllergies().subscribe(
      (result) => {
        this.alergies = result;
      }
    );
  }
  
  addToWishList(product) {
    this.dataService.disAddWishList(product.product_id).subscribe(
      (result) => {
        console.log(result);
        this.refresh()
      });
  }

  addToAlergies(product) {
    this.dataService.disAddAllergy(product.product_id).subscribe(
      (result) => {
        console.log(result);
        this.refresh()
      });
  }

  acceptOrder() {
    this.dataService.disAcceptOrder().subscribe();
  }

  orderRecieved() {
    this.dataService.disOrderRecieved().subscribe();
  }

  logout() {
    this.auth.logout();
  }

}
