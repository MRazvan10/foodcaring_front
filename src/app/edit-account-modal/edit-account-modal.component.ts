import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DataService } from 'src/service/data.service';

@Component({
  selector: 'modal-content',
  templateUrl: './edit-account-modal.component.html',
  styleUrls: ['./edit-account-modal.component.scss']
})
export class EditAccountModalComponent implements OnInit {
  edit:boolean = false;
  editForm: FormGroup;
  username = '';

  constructor(
    public bsModalRef: BsModalRef,
    private fb: FormBuilder,
    private dataService: DataService) {
   }

  ngOnInit(): void {
    this.editForm = this.fb.group({
      role: ['', Validators.required],
      username: [{value: this.username, disabled: this.edit}, Validators.required],
      password: ['', Validators.required]
    });
  }

  submit() {
    const body = this.editForm.value;
    if(this.edit) {
      body.username = this.username;
      this.dataService.editAccount(body).subscribe(
        (result) => {
          console.log(result);
          this.bsModalRef.hide()
        },
        (error) => {
          console.log(error);
        }
      );
    }
    else {
      this.dataService.addAccount(body).subscribe(
        (result) => {
          console.log(result);
          this.bsModalRef.hide()
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

}
