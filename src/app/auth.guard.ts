import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private auth: AuthService, 
    private router: Router
    )
  { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.auth.isLoggedIn()) {
        if( this.auth.getRole() === 'ADMIN') {
          return true;
        }
        else if(this.auth.getRole() === 'MANAGER') {
          if(route.url[0].path === 'admin')
            return false;
          return true;
        } else if (this.auth.getRole() === 'DONNOR') {
          if(route.url[0].path === 'donor')
            return true;
          return false;
        } else if (this.auth.getRole() === 'DISADVANTAGED') {
          if(route.url[0].path === 'disadvantaged')
            return true;
          return false;
        }
      } else {
        //return true; //TESTING CHANGE BACK BEFORE PUSH
        this.router.navigate(['/login']);
        return false;
      }
  }
  
}
